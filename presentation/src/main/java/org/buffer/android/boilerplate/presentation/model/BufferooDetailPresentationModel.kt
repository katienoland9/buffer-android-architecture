package org.buffer.android.boilerplate.presentation.model

/**
 * Representation for a [BufferooDetailPresentationModel] instance for this layers Model representation
 */
class BufferooDetailPresentationModel(val id: String,
                                      val name: String,
                                      val title: String,
                                      val avatar: String,
                                      val description: String)