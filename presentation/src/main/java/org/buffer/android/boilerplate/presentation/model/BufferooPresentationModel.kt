package org.buffer.android.boilerplate.presentation.model

/**
 * Representation for a [BufferooPresentationModel] instance for this layers Model representation
 */
class BufferooPresentationModel(val id: String, val name: String, val title: String, val avatar: String)