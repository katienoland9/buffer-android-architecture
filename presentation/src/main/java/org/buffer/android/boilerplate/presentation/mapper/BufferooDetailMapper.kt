package org.buffer.android.boilerplate.presentation.mapper

import org.buffer.android.boilerplate.domain.model.BufferooDetail
import org.buffer.android.boilerplate.presentation.model.BufferooDetailPresentationModel
import javax.inject.Inject


open class BufferooDetailMapper @Inject constructor(): Mapper<BufferooDetailPresentationModel, BufferooDetail> {

    override fun mapToPresentationModel(type: BufferooDetail): BufferooDetailPresentationModel {
        return BufferooDetailPresentationModel(type.id.toString(), type.name, type.title, type.avatar, type.description)
    }
}