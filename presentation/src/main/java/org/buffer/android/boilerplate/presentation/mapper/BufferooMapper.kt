package org.buffer.android.boilerplate.presentation.mapper

import org.buffer.android.boilerplate.domain.model.Bufferoo
import org.buffer.android.boilerplate.presentation.model.BufferooPresentationModel
import javax.inject.Inject

/**
 * Map a [BufferooPresentationModel] to and from a [Bufferoo] instance when data is moving between
 * this layer and the Domain layer
 */
open class BufferooMapper @Inject constructor(): Mapper<BufferooPresentationModel, Bufferoo> {

    /**
     * Map a [Bufferoo] instance to a [BufferooPresentationModel] instance
     */
    override fun mapToPresentationModel(type: Bufferoo): BufferooPresentationModel {
        return BufferooPresentationModel(type.id.toString(), type.name, type.title, type.avatar)
    }


}