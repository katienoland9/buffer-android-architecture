package org.buffer.android.boilerplate.presentation.detail

import io.reactivex.observers.DisposableSingleObserver
import org.buffer.android.boilerplate.domain.interactor.SingleUseCase
import org.buffer.android.boilerplate.domain.interactor.detail.GetBufferooDetail
import org.buffer.android.boilerplate.domain.model.BufferooDetail
import org.buffer.android.boilerplate.presentation.mapper.BufferooDetailMapper
import javax.inject.Inject

class BufferooDetailPresenter @Inject constructor(
        val bufferooDetailView: BufferooDetailContract.View,
        val getBufferooDetailUseCase: SingleUseCase<BufferooDetail, GetBufferooDetail.Params>,
        val bufferooDetailMapper: BufferooDetailMapper) : BufferooDetailContract.Presenter {

    override fun loadBufferooDetails() {

        getBufferooDetailUseCase.execute(object : DisposableSingleObserver<BufferooDetail>() {
            override fun onSuccess(bufferooDetail: BufferooDetail) {
                bufferooDetailView.hideProgress()
                bufferooDetailView.showBufferooDetails(bufferooDetailMapper.mapToPresentationModel(bufferooDetail))
            }

            override fun onError(e: Throwable) {
                bufferooDetailView.showErrorState(e)
            }

        }, GetBufferooDetail.Params(bufferooDetailView.bufferooId))
    }

    override fun start() {
        loadBufferooDetails()
    }

    override fun stop() {
        getBufferooDetailUseCase.dispose()
    }

}