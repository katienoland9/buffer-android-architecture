package org.buffer.android.boilerplate.presentation.detail

import org.buffer.android.boilerplate.presentation.BasePresenter
import org.buffer.android.boilerplate.presentation.BaseView
import org.buffer.android.boilerplate.presentation.model.BufferooDetailPresentationModel

interface BufferooDetailContract {
    interface View : BaseView <Presenter> {
        var bufferooId: Int?

        fun showProgress()

        fun hideProgress()

        fun showBufferooDetails(bufferoo: BufferooDetailPresentationModel)

        fun showErrorState(e: Throwable)

    }
    interface Presenter : BasePresenter {
        fun loadBufferooDetails()
    }
}