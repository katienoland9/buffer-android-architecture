package org.buffer.android.boilerplate.domain.interactor.detail

import io.reactivex.Single
import org.buffer.android.boilerplate.domain.executor.PostExecutionThread
import org.buffer.android.boilerplate.domain.executor.ThreadExecutor
import org.buffer.android.boilerplate.domain.interactor.SingleUseCase
import org.buffer.android.boilerplate.domain.model.BufferooDetail
import org.buffer.android.boilerplate.domain.repository.BufferooDetailRepository
import javax.inject.Inject

class GetBufferooDetail @Inject constructor(val bufferooRepository: BufferooDetailRepository,
                                            threadExecutor: ThreadExecutor,
                                            postExecutionThread: PostExecutionThread): SingleUseCase<BufferooDetail, GetBufferooDetail.Params>(threadExecutor, postExecutionThread) {
    override fun buildUseCaseObservable(params: Params?): Single<BufferooDetail> {
        return params?.bufferooId?.let {
            bufferooRepository.getBufferooDetails(it)
        } ?: Single.error(IllegalArgumentException("A Bufferoo ID is required"))
    }


    data class Params(val bufferooId: Int?)
}
