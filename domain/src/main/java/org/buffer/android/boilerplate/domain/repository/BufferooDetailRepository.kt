package org.buffer.android.boilerplate.domain.repository

import io.reactivex.Single
import org.buffer.android.boilerplate.domain.model.BufferooDetail

interface BufferooDetailRepository {
    fun getBufferooDetails(id: Int): Single<BufferooDetail>
}