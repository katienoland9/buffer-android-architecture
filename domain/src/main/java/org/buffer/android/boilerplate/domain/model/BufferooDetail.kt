package org.buffer.android.boilerplate.domain.model

data class BufferooDetail(val id: Int,
                          val name: String,
                          val title: String,
                          val avatar: String,
                          val description: String)