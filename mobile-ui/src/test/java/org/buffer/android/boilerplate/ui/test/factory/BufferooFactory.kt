package org.buffer.android.boilerplate.ui.test.factory

import org.buffer.android.boilerplate.presentation.model.BufferooPresentationModel
import org.buffer.android.boilerplate.ui.test.factory.DataFactory.Factory.randomUuid

/**
 * Factory class for Bufferoo related instances
 */
class BufferooFactory {

    companion object Factory {

        fun makeBufferooView(): BufferooPresentationModel {
            return BufferooPresentationModel(randomUuid(), randomUuid(), randomUuid())
        }

    }

}