package org.buffer.android.boilerplate.ui.mapper

import org.buffer.android.boilerplate.presentation.model.BufferooPresentationModel
import org.buffer.android.boilerplate.ui.model.BufferooViewModel
import javax.inject.Inject

/**
 * Map a [BufferooPresentationModel] to and from a [BufferooViewModel] instance when data is moving between
 * this layer and the Domain layer
 */
open class BufferooMapper @Inject constructor(): Mapper<BufferooViewModel, BufferooPresentationModel> {

    /**
     * Map a [BufferooPresentationModel] instance to a [BufferooViewModel] instance
     */
    override fun mapToViewModel(type: BufferooPresentationModel): BufferooViewModel {
        return BufferooViewModel(type.id.toInt(), type.name, type.title, type.avatar)
    }

}