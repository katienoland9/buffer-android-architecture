package org.buffer.android.boilerplate.ui.model

/**
 * Representation for a [BufferooDetailViewModel] fetched from an external layer data source
 */
class BufferooDetailViewModel(val id: Int,
                              val name: String,
                              val title: String,
                              val avatar: String,
                              val description: String)