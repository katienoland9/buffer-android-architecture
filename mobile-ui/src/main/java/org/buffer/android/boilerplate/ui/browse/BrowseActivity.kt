package org.buffer.android.boilerplate.ui.browse

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_browse.*
import org.buffer.android.boilerplate.presentation.browse.BrowseBufferoosContract
import org.buffer.android.boilerplate.presentation.model.BufferooPresentationModel
import org.buffer.android.boilerplate.ui.BaseActivity
import org.buffer.android.boilerplate.ui.R
import org.buffer.android.boilerplate.ui.detail.DetailActivity
import org.buffer.android.boilerplate.ui.mapper.BufferooMapper
import javax.inject.Inject

class BrowseActivity: BaseActivity<BrowseBufferoosContract.Presenter>(), BrowseBufferoosContract.View {

    @Inject override lateinit var presenter: BrowseBufferoosContract.Presenter
    @Inject lateinit var browseAdapter: BrowseAdapter
    @Inject lateinit var mapper: BufferooMapper


    override fun hideProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun showProgress() {
        progress.visibility = View.GONE
    }

    override fun showBufferoos(bufferoos: List<BufferooPresentationModel>) {
        browseAdapter.bufferoos = bufferoos.map { mapper.mapToViewModel(it) }
        browseAdapter.notifyDataSetChanged()
        recycler_browse.visibility = View.VISIBLE
    }

    override fun hideBufferoos() {
        recycler_browse.visibility = View.VISIBLE
    }

    override fun showErrorState() {
    }

    override fun hideErrorState() {
    }

    override fun showEmptyState() {
    }

    override fun hideEmptyState() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_browse)
        setupBrowseRecycler()
    }


    private fun setupBrowseRecycler() {
        recycler_browse.layoutManager = LinearLayoutManager(this)
        browseAdapter.clickHandler = { id ->
            val detailIntent = Intent(this, DetailActivity::class.java)
            detailIntent.putExtra(DetailActivity.EXTRA_BUFFEROO_ID, id)
            startActivity(detailIntent)
        }
        recycler_browse.adapter = browseAdapter

    }

}