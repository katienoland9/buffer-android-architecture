package org.buffer.android.boilerplate.ui.injection.module

import dagger.Module
import dagger.Provides
import org.buffer.android.boilerplate.domain.interactor.detail.GetBufferooDetail
import org.buffer.android.boilerplate.presentation.detail.BufferooDetailContract
import org.buffer.android.boilerplate.presentation.detail.BufferooDetailPresenter
import org.buffer.android.boilerplate.presentation.mapper.BufferooDetailMapper
import org.buffer.android.boilerplate.ui.detail.DetailActivity
import org.buffer.android.boilerplate.ui.injection.scopes.PerActivity

@Module
open class DetailActivityModule {
    @PerActivity
    @Provides
    internal fun detailActivityProvider(detailActivity: DetailActivity): BufferooDetailContract.View = detailActivity

    @PerActivity
    @Provides
    internal fun detailPresenterProvider(bufferooDetailView: BufferooDetailContract.View, getBufferooDetailUseCase: GetBufferooDetail,
                                         bufferooDetailMapper: BufferooDetailMapper):
            BufferooDetailContract.Presenter = BufferooDetailPresenter(bufferooDetailView, getBufferooDetailUseCase, bufferooDetailMapper)
}