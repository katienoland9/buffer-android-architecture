package org.buffer.android.boilerplate.ui.model

/**
 * Representation for a [BufferooViewModel] fetched from an external layer data source
 */
class BufferooViewModel(val id: Int, val name: String, val title: String, val avatar: String)