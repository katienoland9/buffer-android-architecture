package org.buffer.android.boilerplate.ui.detail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_detail.*
import org.buffer.android.boilerplate.presentation.detail.BufferooDetailContract
import org.buffer.android.boilerplate.presentation.model.BufferooDetailPresentationModel
import org.buffer.android.boilerplate.ui.BaseActivity
import org.buffer.android.boilerplate.ui.R
import org.buffer.android.boilerplate.ui.mapper.BufferooDetailMapper
import timber.log.Timber
import javax.inject.Inject

class DetailActivity : BaseActivity<BufferooDetailContract.Presenter>(), BufferooDetailContract.View {

    companion object {
        val EXTRA_BUFFEROO_ID = "ui.detail.extra_bufferoo_id"
    }

    @Inject override lateinit var presenter: BufferooDetailContract.Presenter
    @Inject lateinit var mapper: BufferooDetailMapper

    override var bufferooId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        AndroidInjection.inject(this)
        setupDetailView()
        bufferooId = intent.getIntExtra(EXTRA_BUFFEROO_ID, -1)
    }

    fun setupDetailView() {
        supportActionBar?.title = intent.extras[EXTRA_BUFFEROO_ID].toString()
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
        contentLayout.visibility = View.GONE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
        contentLayout.visibility = View.VISIBLE
    }

    override fun showBufferooDetails(bufferoo: BufferooDetailPresentationModel) {
        with (bufferoo) {
            nameTextView.text = name
            descriptionTextView.text = description
            titleTextView.text = title
            Glide.with(this@DetailActivity)
                    .load(bufferoo.avatar)
                    .apply(RequestOptions.circleCropTransform())
                    .into(avatarImage)
        }
    }

    override fun showErrorState(e: Throwable) {
        Timber.d(e)
        // TODO: launch error activity
        Toast.makeText(this, "An error occured", Toast.LENGTH_LONG).show()
    }
}