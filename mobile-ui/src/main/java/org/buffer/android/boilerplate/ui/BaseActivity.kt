package org.buffer.android.boilerplate.ui

import android.support.v7.app.AppCompatActivity
import org.buffer.android.boilerplate.presentation.BasePresenter
import org.buffer.android.boilerplate.presentation.BaseView

abstract class BaseActivity<T: BasePresenter> : BaseView<T>, AppCompatActivity() {

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }
}