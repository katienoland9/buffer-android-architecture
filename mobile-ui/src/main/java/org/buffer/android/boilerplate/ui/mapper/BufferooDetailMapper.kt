package org.buffer.android.boilerplate.ui.mapper

import org.buffer.android.boilerplate.presentation.model.BufferooDetailPresentationModel
import org.buffer.android.boilerplate.ui.model.BufferooDetailViewModel
import javax.inject.Inject

open class BufferooDetailMapper @Inject constructor(): Mapper<BufferooDetailViewModel, BufferooDetailPresentationModel> {

    override fun mapToViewModel(type: BufferooDetailPresentationModel): BufferooDetailViewModel {
        return BufferooDetailViewModel(type.id.toInt(), type.name, type.title, type.avatar, type.description)
    }
}