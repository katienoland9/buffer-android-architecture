package org.buffer.android.boilerplate.remote.test.factory

import org.buffer.android.boilerplate.remote.BufferooService
import org.buffer.android.boilerplate.remote.model.BufferooRemoteModel
import org.buffer.android.boilerplate.remote.test.factory.DataFactory.Factory.randomUuid

/**
 * Factory class for Bufferoo related instances
 */
class BufferooFactory {

    companion object Factory {

        fun makeBufferooResponse(): BufferooService.BufferooResponse {
            val bufferooResponse = BufferooService.BufferooResponse()
            bufferooResponse.team = makeBufferooModelList(5)
            return bufferooResponse
        }

        fun makeBufferooModelList(count: Int): List<BufferooRemoteModel> {
            val bufferooEntities = mutableListOf<BufferooRemoteModel>()
            repeat(count) {
                bufferooEntities.add(makeBufferooModel())
            }
            return bufferooEntities
        }

        fun makeBufferooModel(): BufferooRemoteModel {
            return BufferooRemoteModel(randomUuid(), randomUuid(), randomUuid())
        }

    }

}