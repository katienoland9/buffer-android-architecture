package org.buffer.android.boilerplate.remote

import io.reactivex.Single
import org.buffer.android.boilerplate.data.model.BufferooDetailEntity
import org.buffer.android.boilerplate.data.model.BufferooEntity
import org.buffer.android.boilerplate.data.repository.browse.BufferooRemote
import org.buffer.android.boilerplate.data.repository.detail.BufferooDetailRemote
import org.buffer.android.boilerplate.remote.mapper.BufferooDetailEntityMapper
import org.buffer.android.boilerplate.remote.mapper.BufferooEntityMapper
import javax.inject.Inject

class BufferooDetailRemoteImpl @Inject constructor(private val bufferooService: BufferooService,
                                                   private val entityMapper: BufferooDetailEntityMapper) :
        BufferooDetailRemote {
    override fun getBufferooDetails(id: Int): Single<BufferooDetailEntity> {
        return bufferooService.getBufferooDetails(id)
                .map {
                    entityMapper.mapFromRemote(it)
                }
    }


}