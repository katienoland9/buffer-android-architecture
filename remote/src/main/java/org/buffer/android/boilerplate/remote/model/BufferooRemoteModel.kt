package org.buffer.android.boilerplate.remote.model

/**
 * Representation for a [BufferooRemoteModel] fetched from the API
 */
class BufferooRemoteModel(val id: Int, val name: String, val title: String, val avatar: String)