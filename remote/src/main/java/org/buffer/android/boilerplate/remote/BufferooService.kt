package org.buffer.android.boilerplate.remote

import io.reactivex.Single
import org.buffer.android.boilerplate.remote.model.BufferooDetailRemoteModel
import org.buffer.android.boilerplate.remote.model.BufferooRemoteModel
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Defines the abstract methods used for interacting with the Bufferoo API
 */
interface BufferooService {

    @GET("bufferoos")
    fun getBufferoos(): Single<List<BufferooRemoteModel>>

    @GET("bufferoos/{id}")
    fun getBufferooDetails(@Path("id") id: Int): Single<BufferooDetailRemoteModel>
}
