package org.buffer.android.boilerplate.remote.model

/**
 * Representation for a [BufferooDetailRemoteModel] fetched from the API
 */
class BufferooDetailRemoteModel(val id: Int, val name: String, val title: String, val avatar: String, val description: String)