package org.buffer.android.boilerplate.remote.mapper

import org.buffer.android.boilerplate.data.model.BufferooEntity
import org.buffer.android.boilerplate.remote.model.BufferooRemoteModel
import javax.inject.Inject

/**
 * Map a [BufferooRemoteModel] to and from a [BufferooEntity] instance when data is moving between
 * this later and the Data layer
 */
open class BufferooEntityMapper @Inject constructor(): EntityMapper<BufferooRemoteModel, BufferooEntity> {

    /**
     * Map an instance of a [BufferooRemoteModel] to a [BufferooEntity] model
     */
    override fun mapFromRemote(type: BufferooRemoteModel): BufferooEntity {
        return BufferooEntity(type.id, type.name, type.title, type.avatar)
    }

}