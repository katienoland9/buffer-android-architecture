package org.buffer.android.boilerplate.data

import io.reactivex.Single
import org.buffer.android.boilerplate.data.mapper.BufferooDetailMapper
import org.buffer.android.boilerplate.data.source.detail.BufferooDetailDataStoreFactory
import org.buffer.android.boilerplate.domain.model.BufferooDetail
import org.buffer.android.boilerplate.domain.repository.BufferooDetailRepository

import javax.inject.Inject


class BufferooDetailDataRepository @Inject constructor(private val factory: BufferooDetailDataStoreFactory,
                                                       private val bufferooMapper: BufferooDetailMapper) :
        BufferooDetailRepository {
    override fun getBufferooDetails(id: Int): Single<BufferooDetail> {
        val dataStore = factory.retrieveDataStore()
        return dataStore.getBufferooDetails(id).map { bufferooMapper.mapFromEntity(it) }

    }

}