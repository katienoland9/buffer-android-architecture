package org.buffer.android.boilerplate.data.mapper

import org.buffer.android.boilerplate.data.model.BufferooDetailEntity
import org.buffer.android.boilerplate.domain.model.BufferooDetail
import javax.inject.Inject


open class BufferooDetailMapper @Inject constructor(): Mapper<BufferooDetailEntity, BufferooDetail> {
    override fun mapToEntity(type: BufferooDetail): BufferooDetailEntity {
        return BufferooDetailEntity(type.id, type.name, type.title, type.avatar, type.description)
    }


    override fun mapFromEntity(type: BufferooDetailEntity): BufferooDetail {
        return BufferooDetail(type.id, type.name, type.title, type.avatar, type.description)
    }
}