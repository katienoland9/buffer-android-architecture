package org.buffer.android.boilerplate.data.source.detail

import org.buffer.android.boilerplate.data.repository.browse.BufferooCache
import org.buffer.android.boilerplate.data.repository.detail.BufferooDetailDataStore
import javax.inject.Inject

/**
 * Create an instance of a BufferooDataStore
 */
open class BufferooDetailDataStoreFactory @Inject constructor(
        private val bufferooCache: BufferooCache,
        private val bufferooDetailRemoteDataStore: BufferooDetailRemoteDataStore) {

    open fun retrieveDataStore(): BufferooDetailDataStore {
        return retrieveRemoteDataStore()
    }

    /**
     * Return an instance of the Cache Data Store
     */
    open fun retrieveRemoteDataStore(): BufferooDetailDataStore {
        return bufferooDetailRemoteDataStore
    }

}