package org.buffer.android.boilerplate.data.model

/**
 * Representation for a [BufferooDetailEntity] fetched from an external layer data source
 */
data class BufferooDetailEntity(val id: Int, val name: String, val title: String, val avatar: String, val description: String)