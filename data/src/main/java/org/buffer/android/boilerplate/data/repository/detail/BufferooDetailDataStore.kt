package org.buffer.android.boilerplate.data.repository.detail

import io.reactivex.Single
import org.buffer.android.boilerplate.data.model.BufferooDetailEntity


interface BufferooDetailDataStore {
    fun getBufferooDetails(id: Int): Single<BufferooDetailEntity>

}