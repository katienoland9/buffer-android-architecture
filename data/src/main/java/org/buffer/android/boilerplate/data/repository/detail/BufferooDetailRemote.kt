package org.buffer.android.boilerplate.data.repository.detail

import io.reactivex.Single
import org.buffer.android.boilerplate.data.model.BufferooDetailEntity


interface BufferooDetailRemote {

    fun getBufferooDetails(id: Int): Single<BufferooDetailEntity>

}