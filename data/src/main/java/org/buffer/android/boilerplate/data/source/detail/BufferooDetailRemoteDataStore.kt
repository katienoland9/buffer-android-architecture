package org.buffer.android.boilerplate.data.source.detail

import io.reactivex.Single
import org.buffer.android.boilerplate.data.model.BufferooDetailEntity
import org.buffer.android.boilerplate.data.repository.browse.BufferooRemote
import org.buffer.android.boilerplate.data.repository.detail.BufferooDetailDataStore
import org.buffer.android.boilerplate.data.repository.detail.BufferooDetailRemote
import javax.inject.Inject


open class BufferooDetailRemoteDataStore @Inject constructor(private val bufferooRemote: BufferooDetailRemote) :
        BufferooDetailDataStore {


    override fun getBufferooDetails(id: Int): Single<BufferooDetailEntity> {
        return bufferooRemote.getBufferooDetails(id)
    }

}