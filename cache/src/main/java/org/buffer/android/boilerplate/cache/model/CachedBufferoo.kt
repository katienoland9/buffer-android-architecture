package org.buffer.android.boilerplate.cache.model

/**
 * Model used solely for the caching of a bufferroo
 */
data class CachedBufferoo(val id: Int, val name: String, val title: String, val avatar: String)